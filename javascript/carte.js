
var carte;

// déclaration des différentes zones :
// il y a une zone par espèce.
// Chaque zone contient 3 variables :
// id est un identifiant unique
// geoId correspond à la zone géographique définie dans le fichier SVG
// nom est le nom de l'espèce tel qu'on veut l'écrire (avec espaces, accents...)
var zoneDefinition = [ {
	"id" : "ble",
	"geoId" : "ble",
	"nom" : "blé"
}, {
	"id" : "mais",
	"geoId" : "mais",
	"nom" : "maïs"
}, {
	"id" : "tomate",
	"geoId" : "tomate",
	"nom" : "tomate"
}, {
	"id" : "carotte",
	"geoId" : "carotte",
	"nom" : "carotte"
}, {
	"id" : "orange",
	"geoId" : "orange",
	"nom" : "orange"
}, {
	"id" : "petitpois",
	"geoId" : "petitpois",
	"nom" : "petit pois"
}, {
	"id" : "artichaut",
	"geoId" : "pourtour_med",
	"nom" : "artichaut"
}, {
	"id" : "orge",
	"geoId" : "orge",
	"nom" : "orge"
}, {
	"id" : "pommedeterre",
	"geoId" : "pommedeterre",
	"nom" : "pomme de terre"
}, {
	"id" : "soja",
	"geoId" : "soja",
	"nom" : "soja"
}, {
	"id" : "tournesol",
	"geoId" : "tournesol",
	"nom" : "tournesol"
}, {
	"id" : "cafe",
	"geoId" : "cafe",
	"nom" : "café"
}, {
	"id" : "cacao",
	"geoId" : "cacao",
	"nom" : "cacao"
}, {
	"id" : "haricot",
	"geoId" : "haricot",
	"nom" : "haricot"
}, {
	"id" : "canneasucre",
	"geoId" : "canneasucre",
	"nom" : "canne à sucre"
}, {
	"id" : "olivier",
	"geoId" : "ble",
	"nom" : "olivier"
}, {
	"id" : "banane",
	"geoId" : "banane",
	"nom" : "banane"
}, {
	"id" : "betterave",
	"geoId" : "betterave",
	"nom" : "betterave"
}, {
	"id" : "coton",
	"geoId" : "coton",
	"nom" : "coton"
} ];

/*
 * , { "id" : "abricot", "geoId" : "abricot", "nom" : "abricot" }, { "id" :
 * "fraise", "geoId" : "fraise", "nom" : "fraise" } , { "id" : "chene", "nom" :
 * "chêne" }
 */

// permet de donner au SVG la taille de son conteneur
function resetSize(svg) {
	svg.configure({
		width : $(svg._container).width(),
		height : $(svg._container).height()
	});
	// $('svg', svg.root()).attr('preserveAspectRatio',"none");
}
/* Callback after loading external document */
function loadDone(svg, error) {
	// si cette fonction est lancée, c'est que le chargement est terminé

	// svg.text(10, 20, error || 'Loaded into ' + this.id);

	// $('#ble', svg.root()).hide();
	// si il y a une erreur, c'est que le SVG n'est pas supporté par le
	// navigateur
	if (error != undefined) {
		// dans ce cas, on crée un div pour afficher un message d'erreur
		var probleme = $("<div class='erreur'>Désolé, ce jeu ne peut pas fonctionner sur votre navigateur car il ne supporte pas le standard SVG... Tout autre navigateur respectant les standards (Firefox, Chromium, Safari... liste non exhaustive) vous permettra d'y jouer.</div>");
		// on place le div d'erreur dans l'élément #depart, en premier
		$("#depart").prepend(probleme);
	}

	// on crée un objet "carte" (défini en dessous)
	carte = new Carte($("#depart"), svg, zoneDefinition);
}

// définition d'un objet "Position", qui contient des coordonnées x et y
function Position(xIn, yIn) {
	this.x = xIn;
	this.y = yIn;
}

// définition de l'objet "carte"
function Carte(div, svgIn, zoneDefinition) {
	var self = this;
	var divDepart = div;
	var divCarte = $("#carte");
	var svg = svgIn
	var root = svgIn.root();
	var zoneList = [];
	var zoneDefinition = zoneDefinition;
	var panneauMessages;
	var panneauBoutons;
	var zoneCourante;
	var message;
	var erreurCarte;
	var imageWidth;
	var imageHeight;
	var divCarteWidth;
	var divCarteHeight;
	var divInfoEspece;
	var cercleFaussePosition;
	var nbErreurs = 0;
	var langueAuChat;

	var getSvgPosition = function(position) {
		var nouvellePosition = new Position();
		var offset = divCarte.offset();
		nouvellePosition.x = position.x - offset.left;
		nouvellePosition.y = position.y - offset.top;
		// OK
		// console.log(divCarteWidth);
		nouvellePosition.x = nouvellePosition.x * (imageWidth / divCarteWidth);
		nouvellePosition.y = nouvellePosition.y
				* (imageHeight / divCarteHeight);
		return nouvellePosition;
	}

	var remiseAzeroErreurs = function() {
		nbErreurs = 0;
		langueAuChat.setAttribute('font-size', '10');
	}

	var erreurEnPlus = function() {
		nbErreurs++;
		langueAuChat.setAttribute('font-size', 10+(nbErreurs*2));
	}

	this.setDivCarteWidth = function(x) {
		divCarteWidth = x;
	}
	this.setDivCarteHeight = function(y) {
		divCarteHeight = y;
	}
	this.getSvg = function() {
		return svg;
	}
	this.getCarteDiv = function() {
		return divCarte;
	}

	this.zoneCliquee = function(zone, e) {
		var message = $("#error", root);
		ecrireSucces("Bravo !!! Vous avez trouvé !", e);

		zone.montrer();
		zone.afficherInformations(e, zoneCourante);
		// console.log("clic dans "+zone.getNom());
	}
	this.pousseBouton = function(zone) {
		remiseAzeroErreurs();
		cacherInformation();
		var message = $("#error", root);
		message.hide();
		if (zoneCourante != undefined) {
			zoneCourante.desactiver();
		}
		zoneCourante = zone;
		panneauMessages.html('');
		erreurCarte.hide();
		zoneCourante.cacher();
		zoneCourante.activer();
	}
	this.getSvgRoot = function() {
		return root;
	}

	var dessinerLesBoutons = function() {
		// les boutons sont définis ici
		// panneauBouton est le conteneur. On l'instancie (on le crée) ici :
		panneauBoutons = $("<div class='panneauBouton'><span class='consigne'>Choisissez une espèce puis cliquez sur la carte pour chercher son origine</span></div>"); // $("contient
		// du html,
		// on peut
		// remplacer
		// div par
		// autre
		// chose si
		// on veut")

		// divDepart est un objet défini à la construction de "Carte" (là haut)
		// :
		// c'est un div aussi, et on peut ajouter dynamiquement des éléments
		// html dedans, comme le panneau bouton que l'on vient de créer
		divDepart.append(panneauBoutons);
		var j = 0;
		for ( var i in zoneList) {
			if (j == 100) {
				j = 0;
				panneauBoutons
						.append($("<hr class='separateur_bandeau'></hr>"));
			}
			j = j + 1
			// on parcourt chaque zone de zoneList (c'est la liste
			// zoneDefinition définie tout en haut)
			// chaque élément de cette liste contient un "id" un "geoid" et un
			// "nom". On utilise le nom pour afficher le nom du légume dans le
			// bouton.
			zone = zoneList[i];
			// on utilise la variable zone pour manipuler la ième zone de la
			// liste
			// zone
			// .setBouton($("<button type='button'></button>")); //ici on peut
			// remplacer le tag HTML button par
			// autre chose, div ou span si on veut.
			zone.setBouton($("<span></span>"));
			panneauBoutons.append(zone.getBouton());

		}
	}

	var ecrireErreur = function(texte, e) {
		panneauMessages.html('');
		// erreur = $("<div class='erreur'>"+texte+"</div>");
		// erreur.css('top', e.pageY);
		// erreur.css('left', e.pageX);
		// panneauMessages.append(erreur);
		var pos = getSvgPosition(new Position(e.pageX, e.pageY));
		erreurCarte.text(texte);
		erreurCarte.show();
		erreurCarte.attr('x', pos.x);
		erreurCarte.attr('y', pos.y);
	}

	var ecrireSucces = function(texte, e) {
		remiseAzeroErreurs();
		erreurCarte.hide();
		panneauMessages.html('');
		boutonRecommencer = $("<a title='Fermer la fenêtre'> × </a>");
		erreur = $("<div class='succes'>" + texte + "</div>");
		erreur.append(boutonRecommencer);

		boutonRecommencer.click(function(e) {
			divInfoEspece.hide();
			boutonRecommencer.hide();
		});

		erreur.css('top', e.pageY);
		erreur.css('left', e.pageX);
		panneauMessages.append(erreur);
	}

	var fondDeCarteClique = function(e) {
		if (zoneCourante != undefined) {
			if (zoneCourante.estCachee()) {
				erreurEnPlus();
				ecrireErreur("Ce n'est pas là...", e);
				// dessiner un cercle rouge à cet endroit :
				var pos = getSvgPosition(new Position(e.pageX, e.pageY));
				cercleFaussePosition.setAttribute('transform', 'translate('
						+ pos.x + '  ' + pos.y + ')');
				cercleFaussePosition.setAttribute('stroke-opacity', 1);
				cercleFaussePosition.setAttribute('cx', 5);
				cercleFaussePosition.style.stroke = 'red';
				$(cercleFaussePosition).animate({
					svgStrokeOpacity : '0',
					svgCx : '1'
				}, 400);

				// console.log(nbErreurs);
			}
		}
	}

	var cacherInformation = function() {
		divInfoEspece.hide();
	}

	var initialize = function() {
		var image = $("image", root);
		imageWidth = image.attr('width');
		imageHeight = image.attr('height');

		svg.text(10, 20, '', {
			id : 'erreurCarte'
		});
		erreurCarte = $("#erreurCarte", root);
		erreurCarte.hide();

		var len = zoneDefinition.length;
		for ( var i = 0; i < len; i++) {
			var zone = new Zone(self, zoneDefinition[i]);
			jQuery.extend(true, zone, zoneDefinition[i]);
			zoneList[zone.id] = zone;
		}

		panneauMessages = $("<div class='panneauMessages'></div>");
		divDepart.prepend(panneauMessages);

		divInfoEspece = $("<div id='infoEspece'></div>");
		divDepart.append(divInfoEspece);
		divInfoEspece.hide();

		dessinerLesBoutons();
		image.click(function(e) {
			fondDeCarteClique(e);
		});

		svg.configure({
			viewBox : '0 0 ' + imageWidth + ' ' + imageHeight,
			preserveAspectRatio : "none"
		});
		self.setDivCarteWidth(parseInt(divCarte.width(), 10));
		self.setDivCarteHeight(parseInt(divCarte.height(), 10));
		panneauMessages.width(divCarteWidth);
		panneauBoutons.width(divCarteWidth);

		cercleFaussePosition = svg.circle(0, 0, 5, {
			fill : 'none',
			stroke : 'red',
			strokeWidth : 3
		});
		cercleFaussePosition.style.stroke = 'none';
		langueAuChat = svg.text(40, 70, "?", {
			fill : 'red'
		});
	}

	initialize();

}
