function Zone(carte, zoneDefinition) {
	var self = this;
	var carte = carte;
	var id = zoneDefinition.id;
	var nom = zoneDefinition.nom;
	var geoId = zoneDefinition.geoId;
	var bouton;

	var svgNode = $('#' + geoId, carte.getSvgRoot());

	svgNode.removeAttr('style');
	$("path", svgNode).removeAttr('style');

	this.activer = function() {
		// console.log("activer");
		svgNode.show();
		bouton.removeClass("boutonChoisi");
		bouton.addClass("boutonChoisi");
	}
	this.montrer = function() {
		svgNode.removeClass("cacher");
		$("path", svgNode).removeClass("cacher");
		svgNode.addClass("montrer");
		$("path", svgNode).addClass("montrer");
	}
	this.cacher = function() {
		svgNode.removeClass("montrer");
		$("path", svgNode).removeClass("montrer");
		svgNode.addClass("cacher");
		$("path", svgNode).addClass("cacher");
	}

	this.estCachee = function() {
		if (svgNode.hasClass("cacher")) {
			return true;
		}
		if ($("path", svgNode).hasClass("cacher")) {
			return true;
		}
		return false;
	}
	this.desactiver = function() {
		// console.log("desactiver");
		svgNode.hide();
		bouton.addClass("boutonPasChoisi");
		bouton.removeClass("boutonChoisi");
	}
	this.getNom = function() {
		return nom;
	}
	this.getBouton = function() {
		return bouton;
	}
	this.setBouton = function(boutonIn) {
		bouton = boutonIn;
		//le bouton que l'on a fabriqué dans Carte => dessinerLesBoutons est reçu ici.
		// on peut lui ajouter un id pour le retrouver en css et lui appliquer une image de fond :
		bouton.attr('id', 'bouton_'+id);//on peut donc référencer dans les CSS #bouton_ble pour accèder au bouton du blé.
		bouton.append($("<span>"+nom+"</span>")); // on rajoute dans le bouton le nom de l'espèce
		
		//si on veut définir une image de fond, on peut le faire avec les CSS et l'id du bouton... mais ce n'est pas dynamique
		// il faudra le mettre à jour à chaque nouvelle espèce...
		// on peut aussi le définir ici de manière dynamique :
		// il suffit de modifier le tableau "zoneDefinition" dans carte.js pour que l'on puisse trouver la bonne image automatiquement
		// les images d'origine à utiliser sont dans doc/légumes.
		// j'ai rangé les images de fonds avec un nom que l'on peut construire avec le nom de la zone :
		// dans images/legumes/carotte_fond.jpg par exemple
		
		//on peut donc aller chercher la bonne image dynamiquement :
		bouton.css('background-image', 'url("./images/legumes/' + id + '_fond.jpg")');
		
		
		//on défini l'action à lancer si on clique sur ce bouton :
		bouton.click(function(e) {
			//on déclenche la fonction "pousseBouton" de Carte avec en argument self (l'objet zone courant) :
			// comme ça la carte peut réagir au clique et savoir sur quel bouton on a cliqué.
			carte.pousseBouton(self);
		});
		bouton.addClass("boutonPasChoisi");//par défaut, au début, le bouton n'est pas choisi (voir les css)
	}

	this.afficherInformations = function(evt, zoneCliquee) {
		$("#infoEspece").hide();
		$('#infoEspece').load('info_espece/' + zoneCliquee.id + '.html div',
				function(response, status, xhr) {
					if (status == "error") {
						$("#infoEspece").hide();
					} else {
						var content = $($("#info_espece").html());
						$("#infoEspece").html(content.html());
						$("#infoEspece").css('top', evt.pageY);
						$("#infoEspece").css('left', evt.pageX);

						var media = $('.media', $("#infoEspece"));
						var hrefImg = $('img', $(media)).attr('href');

						$("#infoEspece").show('slow');
						media.hide();

						//var image = svg.image(100, 50, 200, 200, hrefImg);
						//svg.title(image, 'My image'); 
						//image.show();
						//svg.rect(20, 50, 100, 50, 
						//		{fill: 'yellow', stroke: 'navy', strokeWidth: 5});
					}
				});
	}

	svgNode.hide();
	// svgNode.hover(function(){
	// svgNode.addClass("hover");
	// console.log("dans "+nom);
	// },function(){
	// svgNode.removeClass("hors "+nom);
	// console.log("out");
	// });

	svgNode.click(function(e) {
		carte.zoneCliquee(self, e);
	});
}
